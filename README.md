<H1>Fix for multi-display setups with nvidia on linux laptops</H1>

On laptops with a nvidia graphics card that are connected to an external monitor the frame rate can significantly drop. This tool will prevent this by automaticaly setting the performance mode of the graphics card to max when a display is connected.

This tool requires a working nvidia driver to be present. To test if your driver is working run:

`nvidia-smi`


<H2> Installation </H2>

<H3>Run the following commands to build and install:</H3>

- `git clone git@gitlab.com:Jan_Willem/nvidia-display-boost.git`
- `cd nvidia-display-boost`
- `mkdir build`
-  `cd build`
- `../configure`
- `sudo make install`

<H3>To run and enable startup run:</H3>

- `systemctl --user daemon-reload`
- `systemctl --user enable nvidia-display-boost.service`
- `systemctl --user start nvidia-display-boost.service`

<H2> Developing </H2>

For building [autoreconf](https://wiki.debian.org/Autoreconf) is used.

</H3>To build and install your local version:</H4>

Enter the directory of the repo and run the following commands:

- `autoreconf --install`
- `mkdir build`
- `cd build`
- `../configure`
- `sudo make install`

<H3>To run:</H3>

- `systemctl --user daemon-reload`
- `systemctl --user enable nvidia-display-boost.service`
- `systemctl --user start nvidia-display-boost.service`

<H2>Uninstalling</H2>

Uninstalling can be done using the make command or by manually removing the files. For both ways the systemd service has to be stoped by the user.

<H3> Using the uninstaller:</H3>

Go to the repo directory and run:
- `systemctl --user stop nvidia-display-boost.service`
- `systemctl --user disable nvidia-display-boost.service`
- `sudo make uninstall`

<H3> Manually uninstall</H3>

First run these commands to stop:
- `systemctl --user stop nvidia-display-boost.service`
- `systemctl --user disable nvidia-display-boost.service`

Remove all these files:
- `/usr/lib/systemd/user/nvidia-display-boost.service`
- `/usr/local/bin/nvidia-display-boost`